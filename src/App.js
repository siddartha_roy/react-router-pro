import './App.css';
import AllRoutes from './Routes';
import Menu from './components/Menu';

function App() {
  return (
    <div className="App">
      
      <Menu/>

      <AllRoutes />
    </div>
  );
}

export default App;
