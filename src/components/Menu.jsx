import {Link, useLocation} from 'react-router-dom'

const Menu=()=>{
const location=useLocation()

    return(
        <div className="sidebar">
            <div className='sidebar_items'>
                <Link to='/dashboard' className={location.pathname==='/dashboard'?'sidebar_active':''}>Dashboard</Link>
                <Link to='/tabs' className={location.pathname.includes('/tabs')?'sidebar_active':''}> Profile</Link>
                <Link to='/settings' className={location.pathname==='/settings'?'sidebar_active':''} >settings</Link>


            </div>
        </div>
    )
}

export default Menu