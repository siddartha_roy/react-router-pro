import {Link,useLocation} from 'react-router-dom'

const TabNav=()=>{
    const location=useLocation();


    return(
        <div className="tabnav">
            <Link to='/tabs/tab1' className={location.pathname==='/tabs/tab1'?'tab_active':''} >Badges</Link>
            <Link to='/tabs/tab2' className={location.pathname==='/tabs/tab2'?'tab_active':''}> Skills</Link>
            <Link to='/tabs/tab3' className={location.pathname==='/tabs/tab3'?'tab_active':''} >Experience</Link>
        </div>
    )
}

export default TabNav