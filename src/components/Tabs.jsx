import {Outlet} from 'react-router-dom'
import TabNav from './TabNav'
const Tabs=()=>{
    return(
        <div className="tabs">
            <h1>Your Profile Page</h1>
            <TabNav/>

            <Outlet/>
        </div>
    )
}

export default Tabs